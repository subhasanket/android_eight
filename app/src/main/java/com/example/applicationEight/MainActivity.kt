package com.example.applicationEight

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.application_eight.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    lateinit var notificationManager : NotificationManager
    lateinit var notificationChannel :NotificationChannel
    lateinit var builder:Notification.Builder
    val channelId ="com.example.applicationEight"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notify_btn.setOnClickListener {
            val  intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:383893822"))
            val pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT)
           if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
               notificationChannel = NotificationChannel(channelId,"Hiiii",NotificationManager.IMPORTANCE_HIGH)
               notificationChannel.enableLights(true)
               notificationChannel.lightColor = Color.BLUE
               notificationManager.createNotificationChannel(notificationChannel)
               builder = Notification.Builder(this, channelId)
                   .setSmallIcon(R.drawable.ic_launcher_background)
                   .setContentTitle("textTitle")
                   .setContentText("textContent")
                   .setContentIntent(pendingIntent)
           }else{
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                   builder = Notification.Builder(this, channelId)
                       .setSmallIcon(R.drawable.ic_launcher_background)
                       .setContentTitle("textTitle")
                       .setContentText("textContent")
                       .setContentIntent(pendingIntent)
               }
           }
            notificationManager.notify(1234,builder.build())
        }
    }
}
